<?php

namespace TryCatch\Tests\Addresses\Core\Entities;

use TryCatch\Addresses\Core\Entities\Address;

class AddressTest extends \PHPUnit_Framework_TestCase
{
	public function testAddress()
	{
		$name = "Juarez Mota";
		$phone = "+5511976687928";
		$street = "Rua José Debieux, 282 AP 34";
		$result = Address::create($name, $phone, $street);
		$address = $result->getValue();

		$this->assertSame($name, $address->getName());
		$this->assertSame($phone, $address->getPhone());
		$this->assertSame($street, $address->getStreet());
	}

	public function testNullAddress()
	{
		$result = Address::create(null, null, null);

		$this->assertSame(true, $result->isFailure());
		$this->assertSame("Address name should not be empty, Phone number should not be empty, Street should not be empty", $result->getError());
	}

	public function testEmptyAddress()
	{
		$result = Address::create('', '', '');

		$this->assertSame(true, $result->isFailure());
		$this->assertSame("Address name should not be empty", $result->getError());
	}

    public function testEmptyPhone()
    {
        $result = Address::create('X', '', '');

        $this->assertSame(true, $result->isFailure());
        $this->assertSame("Phone number should not be empty", $result->getError());
    }

    public function testEmptyStreet()
    {
        $result = Address::create('X', 'X', '');

        $this->assertSame(true, $result->isFailure());
        $this->assertSame("Street should not be empty", $result->getError());
    }
}