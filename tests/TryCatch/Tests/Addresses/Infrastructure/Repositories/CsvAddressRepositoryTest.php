<?php

namespace TryCatch\Tests\Addresses\Core\Entities;

use TryCatch\Addresses\Infrastructure\Repositories\CsvAddressRepository;

class CsvAddressRepositoryTest extends \PHPUnit_Framework_TestCase
{
    public function testEmptyAll()
    {
        $repo = new CsvAddressRepository([]);
        $all = [];
        $this->assertSame($all, $repo->all());
    }

    public function testFileAll()
    {
        $filename = __DIR__.DIRECTORY_SEPARATOR.'addresses.csv';
        $repo = new CsvAddressRepository($filename);
        $all = $repo->all();
        $this->assertSame(4, count($all));
        $this->assertSame('Michal', $all[0]->getName());
        $this->assertSame('506088156', $all[0]->getPhone());
        $this->assertSame('Michalowskiego 41', $all[0]->getStreet());
    }

    public function testArrayAll()
    {
        $data = [
            ['A', 'B', 'C']
        ];
        $repo = new CsvAddressRepository($data);
        $all = $repo->all();
        $this->assertSame(1, count($all));
        $this->assertSame('A', $all[0]->getName());
        $this->assertSame('B', $all[0]->getPhone());
        $this->assertSame('C', $all[0]->getStreet());
    }
}