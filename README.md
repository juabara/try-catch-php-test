# PHP Architecture Experiments

The idea of this project is to implement everything without using any libraries or frameworks despite of PHPUnit.

The project itself uses a few known design patterns and architectures:
 
- Domain Driven Design
- Clean Architecture
- MVC
- Routing
- IOC
- REST
- Railway
 
In order to have something to apply all these items the project manages a list of addresses from a csv file.
 
The main goal is to keep everything independent, testable and decoupled.
 
This is a work in progress for my own further reference of how to implement the stuff mentioned earlier.
 
## How to run it
 
```
cd src/TryCatch/Infrastructure/Web/public
```
```
php -S localhost:8000 index.php
```
 
## Pending items and room for improvements
 
- Unit test it 100% + Guzzle for Rest testing
- Better IOC using reflection
- Better routing using regex for parameters