<?php

namespace TryCatch\UseCases;

use TryCatch\Addresses\Core\Boundaries\Provides\AddressRequest;
use TryCatch\Addresses\Core\Boundaries\Requires\IAddressRepository;
use TryCatch\Addresses\Core\Entities\Address;
use TryCatch\Shared\Core\IUseCase;

class NewAddressUseCase implements IUseCase
{
    private $repository;
    private $request;

    public function __construct(IAddressRepository $repository, AddressRequest $request)
    {
        $this->repository = $repository;
        $this->request = $request;
    }

    public function execute()
    {
        $result = Address::create(
                    $this->request->getName(),
                    $this->request->getPhone(),
                    $this->request->getStreet()
                );

        return $result;
    }
}