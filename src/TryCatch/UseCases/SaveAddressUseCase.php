<?php

namespace TryCatch\UseCases;

use TryCatch\Addresses\Core\Boundaries\Provides\AddressRequest;
use TryCatch\Addresses\Core\Boundaries\Requires\IAddressRepository;
use TryCatch\Shared\Core\IUseCase;

class SaveAddressUseCase implements IUseCase
{
    private $repository;
    private $request;
    private $id;

    public function __construct(IAddressRepository $repository, $id, AddressRequest $request)
    {
        $this->repository = $repository;
        $this->id = $id;
        $this->request = $request;
    }

    public function execute()
    {
        $result = $this->repository->save($this->id, $this->request);
        return $result;
    }
}