<?php

namespace TryCatch\UseCases;

use TryCatch\Addresses\Core\Boundaries\Requires\IAddressRepository;
use TryCatch\Shared\Core\IUseCase;

class DeleteAddressUseCase implements IUseCase
{
    private $repository;
    private $id;

    public function __construct(IAddressRepository $repository, $id)
    {
        $this->repository = $repository;
        $this->id = $id;
    }

    public function execute()
    {
        $result = $this->repository->delete($this->id);
        return $result;
    }
}