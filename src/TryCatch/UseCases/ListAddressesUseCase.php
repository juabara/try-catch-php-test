<?php

namespace TryCatch\UseCases;

use TryCatch\Addresses\Core\Boundaries\Requires\IAddressRepository;
use TryCatch\Shared\Core\IUseCase;

class ListAddressesUseCase implements IUseCase
{
    private $repository;

    public function __construct(IAddressRepository $repository)
    {
        $this->repository = $repository;
    }

    public function execute()
    {
        return $this->repository->all();
    }
}