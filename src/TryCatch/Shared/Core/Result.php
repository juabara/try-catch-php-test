<?php

namespace TryCatch\Shared\Core;

class Result
{
	private $logic;
	
	private $value;
	
	private function __construct($isFailure, $error, $value = null)
	{
		$this->logic = new ResultCommonLogic($isFailure, $error);
		$this->value = $value;
	}
	
	public static function ok($value = null)
	{
		return new Result(false, null, $value);
	}
	
	public static function fail($error)
	{
		return new Result(true, $error, null);
	}

    public static function combine(array $results)
    {
        $errors = [];

        foreach ($results as $result) {
            if ($result->isFailure())
                $errors[] = $result->getError();
        }

        if (count($errors) == 0)
            return Result::ok();

        $errorMessage = implode(', ', $errors);
        return Result::fail($errorMessage);
    }

	public function getValue()
	{
		return $this->value;
	}

	public function isFailure()
	{
		return $this->logic->isFailure();
	}

	public function isSuccess()
	{
		return $this->logic->isSuccess();
	}

	public function getError()
	{
		return $this->logic->getError();
	}

    public function onSuccess(\Closure $func)
    {
        if ($this->isFailure())
            return $this;

        return Result::ok($func($this->getValue()));
    }

    public function onBoth(\Closure $func)
    {
        return $func($this->getValue());
    }

    public function ensure(\Closure $predicate, $errorMessage)
    {
        if ($this->isFailure())
            return $this;

        if (!$predicate())
            return Result::fail($errorMessage);

        return Result::ok($this->getValue());
    }

    public static function toResult($value, $errorMessage)
    {
        if (!isset($value))
            return Result::fail($errorMessage);

        return Result::ok($value);
    }

    public function map(\Closure $func)
    {
        if ($this->isFailure())
            return $this;

        return Result::ok($func());
    }
}
