<?php

namespace TryCatch\Shared\Core;

class ResultCommonLogic
{
    private $isFailure;
    private $error;

    public function __construct($isFailure, $error)
    {
        if ($isFailure && !isset($error))
        {
            throw new \InvalidArgumentException("There must be error message for failure.");
        }

        if (!$isFailure && isset($error))
        {
            throw new \InvalidArgumentException("There should be no error message for success.");
        }

        $this->isFailure = $isFailure;
        $this->error = $error;
    }

    public function isFailure()
    {
        return $this->isFailure;
    }

    public function isSuccess()
    {
        return !$this->isFailure();
    }

    public function getError()
    {
        if ($this->isSuccess()) {
            throw new \LogicException("There is no error message for success.");
        }

        return $this->error;
    }
}