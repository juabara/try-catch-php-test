<?php

namespace TryCatch\Shared\Core;

interface IUseCase
{
    public function execute();
}