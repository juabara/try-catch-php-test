<?php

namespace TryCatch\Addresses\Core\Entities;

use TryCatch\Shared\Core\Entity;
use TryCatch\Shared\Core\Result;

class Address extends Entity
{
	private $name;
	private $phone;
	private $street;
	
	private function __construct($name, $phone, $street)
	{
		$this->name = $name;
		$this->phone = $phone;
		$this->street = $street;
	}
	
	public static function create($name, $phone, $street)
	{
        $nameResult = Result::toResult($name, "Address name should not be empty");
        $phoneResult = Result::toResult($phone, "Phone number should not be empty");
        $streetResult = Result::toResult($street, "Street should not be empty");

        $result = Result::combine([$nameResult, $phoneResult, $streetResult]);
        
		return $result->onSuccess(function() use ($name) { return trim($name); })
                      ->onSuccess(function() use ($phone) { return trim($phone); })
                      ->onSuccess(function() use ($street) { return trim($street); })
					  ->ensure(function() use ($name) { return !empty($name); }, "Address name should not be empty")
                      ->ensure(function() use ($phone) { return !empty($phone); }, "Phone number should not be empty")
                      ->ensure(function() use ($street) { return !empty($street); }, "Street should not be empty")
                      ->map(function() use ($name, $phone, $street) { return new Address($name, $phone, $street); });
	}
	
	public function getName()
	{
		return $this->name;
	}
	
	public function getPhone()
	{
		return $this->phone;
	}
	
	public function getStreet()
	{
		return $this->street;
	}
}