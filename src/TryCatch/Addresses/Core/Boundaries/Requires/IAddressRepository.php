<?php

namespace TryCatch\Addresses\Core\Boundaries\Requires;

use TryCatch\Addresses\Core\Boundaries\Provides\AddressRequest;

interface IAddressRepository
{
	public function all();
    public function find($id);
	public function save($id, AddressRequest $request);
    public function add(AddressRequest $request);
    public function delete($id);
}