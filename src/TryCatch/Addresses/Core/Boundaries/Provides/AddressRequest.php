<?php

namespace TryCatch\Addresses\Core\Boundaries\Provides;

class AddressRequest
{
    private $name;
    private $phone;
    private $street;

    public function __construct($name, $phone, $street)
    {
        $this->name = $name;
        $this->phone = $phone;
        $this->street = $street;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getPhone()
    {
        return $this->phone;
    }

    public function getStreet()
    {
        return $this->street;
    }
}