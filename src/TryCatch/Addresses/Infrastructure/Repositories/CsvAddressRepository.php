<?php

namespace TryCatch\Addresses\Infrastructure\Repositories;

use TryCatch\Addresses\Core\Boundaries\Provides\AddressRequest;
use TryCatch\Addresses\Core\Boundaries\Requires\IAddressRepository;
use TryCatch\Addresses\Core\Entities\Address;
use TryCatch\Shared\Core\Result;

class CsvAddressRepository implements IAddressRepository
{
    protected $addresses;
    protected $filename;

    public function __construct($filename = __DIR__.DIRECTORY_SEPARATOR.'addresses.csv')
    {
        $addresses = [];

        if (is_string($filename))
            $addresses = $this->readAddresses($filename);

        if (is_array($filename))
            $addresses = $filename;

        $this->loadAddresses($addresses);
    }

    public function all()
    {
        return $this->addresses;
    }

    private function loadAddresses(array $addresses)
    {
        $this->addresses = [];

        foreach ($addresses as $line)
        {
            $addressResult = Address::create($line[0], $line[1], $line[2]);
            if ($addressResult->isSuccess())
                $this->addresses[] = $addressResult->getValue();
        }
    }

    private function readAddresses($filename)
    {
        $file = fopen($filename, 'r');

        $addresses = [];

        while (($line = fgetcsv($file)) !== FALSE)
        {
            $addresses[] = $line;
        }

        fclose($file);

        return $addresses;
    }

    public function save($id, AddressRequest $request)
    {
        $result = $this->find($id);

        if ($result->isFailure())
            return $result;

        $result = Address::create(
            $request->getName(),
            $request->getPhone(),
            $request->getStreet()
        );

        return $result;
    }

    public function add(AddressRequest $request)
    {
        $address = Address::create($request->getName(), $request->getPhone(), $request->getStreet());

        if ($address->isSuccess()) {
            $this->addresses[] = $address->getValue();
        }

        return $address;
    }

    public function find($id)
    {
        if (!array_key_exists($id, $this->addresses))
            return Result::fail('Address not found');

        $address = $this->addresses[$id];

        return Result::ok($address);
    }

    public function delete($id)
    {
        $result = $this->find($id)
                       ->onSuccess(function($address) use ($id) {
                           unset($this->addresses[$id]);
                           return $address;
                       });
        return $result;
    }
}