<?php

use TryCatch\Addresses\Core\Boundaries\Provides\AddressRequest;
use TryCatch\Infrastructure\Web\IoC;

IoC::register('addresses', function()
{
    $repository = new \TryCatch\Addresses\Infrastructure\Repositories\CsvAddressRepository();
    $useCase = new \TryCatch\UseCases\ListAddressesUseCase($repository);
    $controller = new \TryCatch\Infrastructure\Web\Controllers\AddressController($useCase);
    return $controller;
    
});

IoC::register('getAddress', function()
{
    $repository = new \TryCatch\Addresses\Infrastructure\Repositories\CsvAddressRepository();
    $useCase = new \TryCatch\UseCases\GetAddressUseCase($repository, $_GET['id']);
    $controller = new \TryCatch\Infrastructure\Web\Controllers\AddressController($useCase);
    return $controller;

});

IoC::register('newAddress', function()
{
    $repository = new \TryCatch\Addresses\Infrastructure\Repositories\CsvAddressRepository();
    $body = json_decode(file_get_contents('php://input'));
    $request = new AddressRequest($body->name, $body->phone, $body->street);
    $useCase = new \TryCatch\UseCases\NewAddressUseCase($repository, $request);
    $controller = new \TryCatch\Infrastructure\Web\Controllers\AddressController($useCase);
    return $controller;
});

IoC::register('saveAddress', function()
{
    $repository = new \TryCatch\Addresses\Infrastructure\Repositories\CsvAddressRepository();
    $body = json_decode(file_get_contents('php://input'));
    $request = new AddressRequest($body->name, $body->phone, $body->street);
    $useCase = new \TryCatch\UseCases\SaveAddressUseCase($repository, $_GET['id'], $request);
    $controller = new \TryCatch\Infrastructure\Web\Controllers\AddressController($useCase);
    return $controller;
});

IoC::register('deleteAddress', function()
{
    $repository = new \TryCatch\Addresses\Infrastructure\Repositories\CsvAddressRepository();
    $useCase = new \TryCatch\UseCases\DeleteAddressUseCase($repository, $_GET['id']);
    $controller = new \TryCatch\Infrastructure\Web\Controllers\AddressController($useCase);
    return $controller;

});