<?php

require_once 'autoload.php';
require_once 'bootstrap.php';

use TryCatch\Infrastructure\Web\Response;
use TryCatch\Infrastructure\Web\Route;
use TryCatch\Infrastructure\Web\Routes;

Routes::register();

$route = Route::resolve();

if ($route->isFailure()) {
    return Response::notFound();
}

$run = $route->getValue();

if (is_callable($run)) {
    $run();
}
