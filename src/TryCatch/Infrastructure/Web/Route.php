<?php

namespace TryCatch\Infrastructure\Web;

use TryCatch\Shared\Core\Result;

class Route
{
    private static $getRoutes;
    private static $postRoutes;
    private static $putRoutes;
    private static $deleteRoutes;

    public static function get($route, \Closure $func)
    {
        self::add($route, self::$getRoutes, $func);
    }

    public static function post($route, \Closure $func)
    {
        self::add($route, self::$postRoutes, $func);
    }

    public static function put($route, \Closure $func)
    {
        self::add($route, self::$putRoutes, $func);
    }

    public static function delete($route, \Closure $func)
    {
        self::add($route, self::$deleteRoutes, $func);
    }

    private static function add($name, &$routes, \Closure $func)
    {
        if (!isset($routes))
            $routes = [];

        $routes[$name] = $func;
    }

    public static function resolve()
    {
        $verb = $_SERVER['REQUEST_METHOD'];

        $routes = [
            'GET' => self::$getRoutes,
            'POST' => self::$postRoutes,
            'PUT' => self::$putRoutes,
            'DELETE' => self::$deleteRoutes,
        ];

        if (!array_key_exists($verb, $routes))
            return Result::fail('Route not found');

        $router = $routes[$verb];

        $segments = explode('?',$_SERVER["REQUEST_URI"]);
        $routeName = array_shift($segments);

        if (!array_key_exists($routeName, $router))
            return Result::fail('Route not found');

        return Result::ok($router[$routeName]);
    }
}