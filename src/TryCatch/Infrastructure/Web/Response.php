<?php

namespace TryCatch\Infrastructure\Web;

class Response
{
    public static function json($content, $code = 200)
    {
        header('Content-type:application/json;charset=utf-8');
        http_response_code($code);
        echo json_encode($content);
    }

    public static function notFound()
    {
        header('Content-type:application/json;charset=utf-8');
        http_response_code(404);
    }
}