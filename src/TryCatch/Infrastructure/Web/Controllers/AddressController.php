<?php

namespace TryCatch\Infrastructure\Web\Controllers;

use TryCatch\Infrastructure\Web\Response;
use TryCatch\Shared\Core\IUseCase;

class AddressController
{
    private $useCase;

    public function __construct(IUseCase $useCase)
    {
        $this->useCase = $useCase;
    }

    public function all()
    {
        $all = $this->useCase->execute();
        $addresses = array_map(function($item) {
            return $this->toObject($item);
        }, $all);
        return Response::json($addresses);
    }

    private function toObject($address)
    {
        $item = new \stdClass();
        $item->name = $address->getName();
        $item->phone = $address->getPhone();
        $item->street = $address->getStreet();
        return $item;
    }

    private function execute()
    {
        $result = $this->useCase->execute();

        if ($result->isFailure())
            return Response::json($result->getError(), 400);

        $address = $result->getValue();
        return Response::json($this->toObject($address));
    }

    public function get()
    {
        return $this->execute();
    }

    public function post()
    {
        return $this->execute();
    }

    public function put()
    {
        return $this->execute();
    }

    public function delete()
    {
        return $this->execute();
    }
}