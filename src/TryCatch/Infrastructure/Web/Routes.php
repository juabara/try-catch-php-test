<?php

namespace TryCatch\Infrastructure\Web;

use TryCatch\Infrastructure\Web;

class Routes
{
    public static function register()
    {
        Route::get('/addresses', function() {
            $controller = IoC::resolve('addresses');
            return $controller->all();
        });

        Route::get('/address', function() {
            $controller = IoC::resolve('getAddress');
            return $controller->get();
        });

        Route::post('/address', function() {
            $controller = IoC::resolve('newAddress');
            return $controller->post();
        });

        Route::put('/address', function() {
            $controller = IoC::resolve('saveAddress');
            return $controller->put();
        });

        Route::delete('/address', function() {
            $controller = IoC::resolve('deleteAddress');
            return $controller->delete();
        });
    }

}